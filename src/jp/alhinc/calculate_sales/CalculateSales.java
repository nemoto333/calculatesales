package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品コードファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NAME_NON_SEQUENCE = "売上ファイル名が連番になっていません";
	private static final String SALES_AMOUNT_OVER_DIGIT = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//エラー処理3－1
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$",
				"支店" + FILE_NOT_EXIST ,"支店" + FILE_INVALID_FORMAT)) {
			return;
		}

		//商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,
				"^[0-9a-zA-Z]{8}$", "商品" + FILE_NOT_EXIST,"商品" + FILE_INVALID_FORMAT)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			//エラー処理にて条件追加（isFile)
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//エラー処理2－1
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if (latter != (former + 1)) {
				System.out.println(FILE_NAME_NON_SEQUENCE);
				return;
			}
		}

		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size();i++ ) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				List<String> rcdList = new ArrayList<>();
				String line;

				while ((line = br.readLine()) != null) {
					rcdList.add(line);
				}

				//エラー処理2-3,2-4,2-5
				String fileName = rcdFiles.get(i).getName(); //i番目のファイル名を取得
				String getBranchCode = rcdList.get(0); //読み込んだリストから支店コードを取得
				String getCommodityCode = rcdList.get(1); //読み込んだリストから商品コードを取得
				String getSale = rcdList.get(2); //読み込んだリストから売上を取得

				if(!branchNames.containsKey(getBranchCode)) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				if(!commodityNames.containsKey(getCommodityCode)) {
					System.out.println(fileName +"の商品コードが不正です");
					return;
				}

				if(rcdList.size() != 3) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				//エラー処理3-2
				if(!getSale.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//エラー処理2－2
				Long fileSale = Long.parseLong(getSale);
				Long saleAmount = (branchSales.get(getBranchCode)) + fileSale;
				Long commodityAmount = (commoditySales.get(getCommodityCode) + fileSale);

				if((saleAmount >= 1000000000L) || (commodityAmount >= 1000000000L)) {
					System.out.println(SALES_AMOUNT_OVER_DIGIT);
					return;
				}
				branchSales.put(getBranchCode, saleAmount);
				commoditySales.put(getCommodityCode, commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales,String match, String existError ,String matchError) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//エラー処理1-1 1-3
			if(!file.exists()) {
				System.out.println(existError);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				System.out.println(line);
				String[] items = line.split(",");

				//エラー処理1-2 1-4
				if((items.length != 2) || (!items[0].matches(match))) {
					System.out.println(matchError);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File (path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
